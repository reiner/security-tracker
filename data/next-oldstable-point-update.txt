CVE-2023-5157
	[bullseye] - galera-4 26.4.14-0+deb11u1
CVE-2021-32718
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-32719
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-22116
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2018-1279
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-3654
	[bullseye] - nova 2:22.2.2-1+deb11u1
CVE-2022-27240
	[bullseye] - glewlwyd 2.5.2-2+deb11u3
CVE-2022-29967
	[bullseye] - glewlwyd 2.5.2-2+deb11u3
CVE-2023-49208
	[bullseye] - glewlwyd 2.5.2-2+deb11u3
CVE-2021-24119
	[bullseye] - mbedtls 2.16.12-0+deb11u1
CVE-2021-44732
	[bullseye] - mbedtls 2.16.12-0+deb11u1
CVE-2022-32096
	[bullseye] - rhonabwy 0.9.13-3+deb11u2
CVE-2022-2996
	[bullseye] - python-scciclient 0.8.0-2+deb11u1
CVE-2022-24859
	[bullseye] - pypdf2 1.26.0-4+deb11u1
CVE-2023-36810
	[bullseye] - pypdf2 1.26.0-4+deb11u1
CVE-2020-22218
	[bullseye] - libssh2 1.9.0-2+deb11u1
CVE-2022-22995
	[bullseye] - netatalk 3.1.12~ds-8+deb11u2
